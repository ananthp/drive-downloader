Drive Downloader
===============

Script and lib to download files from Google Drive.

Python Setup
------------

The project uses `Poetry`. May need to run `poetry install`.

Run `poetry shell` to activate the venv with all dependencies installed. From here the example script may be run.

Credentials
-----------

Using this lib or running the example script requires some setup on Google Cloud:
- [x] Create a project on Google Cloud Platform
- [x] API and Services > Library, add Google Drive API
- [x] API and Services > Credentials
  - [x] create API Key
  - [x] create OAuth 2.0 Client ID
  - [x] download OAuth client, download json.
  - [x] rename the downloaded json to `credentials.json` and move it to project root.

Note to self: These are all already done for my stuff. `credentials.json` is not checked into git. Therefore, on a fresh clone, I need to locate the project, navigate to credentials, download the json, rename and place it under project root as mentioned above.

Running the script launches the web browser and starts Google authentication. When successfully authenticated, creates a `token.json`. Sometimes we need to delete this `token.json` to trigger fresh authentication. (`token.json` is not checked into git either).

Example
-------

`example.py` describes and demonstrates the capabilities of the lib. By editing and commenting/uncommenting various sections, one can view and download files (or complete directories) on one's google drive.
