"""Download files from Google Drive.

DriveDownloader lets you download:
- individual files.
- directory with all files.
  - nested directories are not supported yet.
- all directories (and their contents) shared by a user.

Sample executable programs can be found at `samples`.
They can serve as utility programs to download stuff from Google Drive.
Be sure to edit the programs before running.
"""

__version__ = "0.3.0"

import os.path
from pathlib import Path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload

# If modifying these scopes, delete the file token.json.
SCOPES = [
    "https://www.googleapis.com/auth/drive.readonly",
]


class DriveDownloader(object):
    """Download files and dirs from Google Drive."""

    FIELDS_ALL = "*"
    FIELDS_ESSENTIAL = "id,name,kind,webViewLink,mimeType"
    FIELDS_MULTIPAGE = "nextPageToken, files(id,name,kind,webViewLink,mimeType)"

    def __init__(self):
        """Setup credentials."""
        creds = None

        # The file token.json stores the user's access and refresh tokens,
        # and is created automatically when the authorization flow completes
        # for the first time.
        if os.path.exists("token.json"):
            creds = Credentials.from_authorized_user_file("token.json", SCOPES)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    "credentials.json", SCOPES
                )
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open("token.json", "w") as token:
                token.write(creds.to_json())

        self.service = build("drive", "v3", credentials=creds)

    def _form_query_str_dirs_by_user(self, user: str) -> str:
        assert user != ""
        return f"'{user}' in owners and mimeType contains 'folder'"

    def _run_query(
        self,
        query: str,
        fields=FIELDS_MULTIPAGE,
        limit=100,
    ) -> dict:
        """Query files/dirs and get result dict.

        query: str -- query string.
               see [Search for files and folders](https://developers.google.com/drive/api/v3/search-files)
               for supported syntax.
        fields: desired fields in the result. defaults to all fields ("*")
                `nextPageToken` is required at the minimum.
                use one of the predefined strings.
        limit: number of results limited by this value. (1-1000, default: 100)
               (This is per page. Next page can be retireved,
                but not implemented yet).
        """
        results = (
            self.service.files()
            .list(
                pageSize=limit,
                supportsAllDrives=True,
                includeItemsFromAllDrives=True,
                fields=fields,
                q=query,
            )
            .execute()
        )
        return results.get("files", [])

    def _fetch_files_list_under_dir(self, dir_id: str):
        assert dir_id != ""
        QUERY = f"'{dir_id}' in parents"
        return self._run_query(QUERY)

    def _get_name(self, id):
        """Query and return the file/dir name based on a Google Drive ID."""
        meta = (
            self.service.files().get(fileId=id, fields=self.FIELDS_ESSENTIAL).execute()
        )
        return meta.get("name")

    def _get_dirs_by_user(self, user: str) -> dict:
        """Query dirs owned/shared by a user and get the response dict."""
        query = self._form_query_str_dirs_by_user(user)
        return self._run_query(query)

    def list_dirs_by_user(self, user: str):
        """Print the list of directories by a user.

        The user could be us, or somebody who shared stuff with us.
        user -- search string. Can contain partial name or email address.
                case insensitive.
        """
        for dir in self._get_dirs_by_user(user):
            id = dir.get("id")
            name = dir.get("name")
            mime = dir.get("mimeType")
            print(f"{id}\t{mime}\t{name}")

    def list_dir(self, dir_id):
        """Print the contents of a dir."""
        for file in self._fetch_files_list_under_dir(dir_id):
            id = file.get("id")
            name = file.get("name")
            print(f"{id} {name}")

    def download_file(
        self,
        **kwargs,
    ):
        """Download a file.

        keyword arguments:
        file_id: str -- just the ID of the file.
        file_meta: dict -- metadata of a file retrieved using a query earlier.
        file_id and file_meta are mutually exclusive. privide only one.

        output_file_full_path: Path -- output file name along with full path
        output_dir: Path -- output directory. File name as per google drive.
        These two are mutually exclusive. Supply one.

        overwrite: bool -- should overwrite if file exists? (default: False)
        """
        file_id = kwargs.get("file_id")
        output_file_full_path: str = kwargs.get("output_file_full_path")
        file_meta: dict = kwargs.get("file_meta")
        output_dir: str = kwargs.get("output_dir")
        overwrite: bool = kwargs.get("overwrite", False)

        if file_id and output_file_full_path:
            # perect. evrything in place
            pass
        elif file_meta and output_dir:
            # ok, we have necessary details
            output_file_full_path = Path(output_dir) / file_meta["name"]
            file_id = file_meta["id"]
        elif file_id and output_dir:
            # don't know the filename yet
            file_name = self._get_name(file_id)
            output_file_full_path = Path(output_dir) / file_name
        else:
            raise Exception("Use one of the file and output params combos")

        output_file_full_path = output_file_full_path.resolve()

        # create parent dir(s) if missing
        parent = output_file_full_path.parent
        if not parent.exists():
            parent.mkdir(parents=True, exist_ok=True)

        if output_file_full_path.exists() and not overwrite:
            print(f"[skip] {output_file_full_path} already exists.")
            return

        request = self.service.files().get_media(fileId=file_id)
        with open(output_file_full_path, "wb") as fh:
            downloader = MediaIoBaseDownload(fh, request)
            print(f"Download: {output_file_full_path.absolute()}", end=" -- ")

            status, done = 0, False
            while done is False:
                status, done = downloader.next_chunk()
                print(f"{int(status.progress()*100)}%", end=" ")

        print("-- Done")

    def download_dir(
        self,
        dir_id: str = None,
        dir_meta: dict = None,
        download_to: str = "/tmp",
    ):
        """Download a directory with all its contents.

        provide dir_id or dir_meta.
        dir_id can be found from the Google Drive link to the dir.
        dir_meta is a dict with metadata, fetched though other API calls.
        download_to is the base dir. A subdir under it will be created.
        """
        if dir_id:
            dir_name = self._get_name(dir_id)
        elif dir_meta:
            dir_id = dir_meta.get("id")
            dir_name = dir_meta.get("name")

        output_dir = Path(download_to) / dir_name
        output_dir.mkdir(
            parents=True,
            exist_ok=True,
        )

        # query dir contents
        files = self._fetch_files_list_under_dir(dir_id)

        # download files
        for file in files:
            self.download_file(file_meta=file, output_dir=output_dir)

    def download_dirs_by_user(self, user: str, download_to: str):
        """Download all directories by a user.

        user: str -- Search term for user. Could be name. Not necessarily full.
              Case insensitive. Or could be email.
        download_to: str -- Directory to download to. Subdirs will be created.
        """
        query = self._form_query_str_dirs_by_user(user)
        dirs = self._run_query(query)
        for dir in dirs:
            self.download_dir(dir_meta=dir, download_to=download_to)
