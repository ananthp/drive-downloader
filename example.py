#!/usr/bin/env python3

"""This script demonstrates the use of Drive Downloader.

Edit and uncomment relevant sections as necessary.
"""

import drive_downloader

# from pprint import pprint

# We want to download stuff shared by a specific user.
# SHARER is a search term for the user.
# Can be that user's name (not necessarily full; case insensitive), or email
SHARER = "john"

# This is the destination for downloads.
# Subdirs will be created as necessary.
DOWNLOAD_ROOT_DIR = "/tmp"

# Instantiate DriveDownloader.
# This will trigger authentication if required.
dd = drive_downloader.DriveDownloader()

# Get the list of dirs and inspect.
dd.list_dirs_by_user(user=SHARER)

# Everything looks good? Download EVERYTHING!
# dd.download_dirs_by_user(user=SHARER, download_to=DOWNLOAD_ROOT_DIR)


# Alternatively, get the list as python dict.
# May clean up, filter and download stuff selectively.
#
result = dd._get_dirs_by_user(user=SHARER)

##################################################
# files under a directory
##################################################

dir_meta = result[0]  # metadata of a directory
dir_id = result[0].get("id")  # just the id of a directory
# or
# dir_id = "some-id-fetched-from-google-drive-website"


# view contents of the dir
dd.list_dir(dir_id)

# optional: inspect the metadata closely
# files_list_dict = dd._fetch_files_list_under_dir(dir_id)
# pprint(files_list_dict)

##################################################
# download all files in a dir
##################################################
# dd.download_dir(
#     dir_id=dir_id,  # or
#     # dir_meta=dir_meta
#     download_to='/tmp/some/path'
# )


##################################################
# Download a single file
##################################################
# dd.download_file(
#     file_id='some-file-id',
#     output_dir='/tmp/test'
# )
